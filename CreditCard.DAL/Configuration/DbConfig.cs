﻿using System.Configuration;

namespace CreditCard.DAL.Configuration
{
    public static class DbConfig
    {
        public static int? CommandTimeout
        {
            get
            {
                int commandTimeout;
                var isOk = int.TryParse(ConfigurationManager.AppSettings["CreditCardContext.CommandTimeout"],
                    out commandTimeout);
                return isOk ? commandTimeout : 120;
            }
        }
    }
}
