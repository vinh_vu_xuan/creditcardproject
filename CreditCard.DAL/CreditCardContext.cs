using CreditCard.DAL.Configuration;

namespace CreditCard.DAL
{
    using System.Data.Entity;

    public partial class CreditCardContext : DbContext
    {
        public CreditCardContext()
            : base("CreditCardContext")
        {
            Database.CommandTimeout = DbConfig.CommandTimeout;
        }

        public virtual DbSet<CreditCard> CreditCards { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CreditCard>()
                .Property(e => e.CardNumber)
                .IsUnicode(false);

            modelBuilder.Entity<CreditCard>()
                .Property(e => e.ExpiredDate)
                .IsUnicode(false);
        }
    }
}
