﻿using CreditCard.Core.Validator;

namespace CreditCard.ServiceCtrs.Infrastructure
{
    public interface IBaseService<in TModel> where TModel : class
    {
        ServiceResult Get(TModel model);
    }
}
