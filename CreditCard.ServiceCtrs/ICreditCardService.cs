﻿using CreditCard.Core.Validator;
using CreditCard.Model;
using CreditCard.ServiceCtrs.Infrastructure;

namespace CreditCard.ServiceCtrs
{
    public interface ICreditCardService : IBaseService<CreditCardModel>
    {
        ServiceResult Get(CreditCardModel model);
    }
}
