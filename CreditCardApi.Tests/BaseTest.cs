﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CreditCard.Repository;
using Moq;

namespace CreditCardApi.Tests
{
    public abstract class BaseTest
    {
        protected Mock<DbSet<T>> MockDbSetSecurable<T>(IQueryable<T> data) where T : class
        {
            var mockDbSet = new Mock<DbSet<T>>();
            mockDbSet.As<IQueryable<T>>().Setup(m => m.Provider).Returns(data.Provider);
            mockDbSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(data.Expression);
            mockDbSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockDbSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            //if (predicate != null)
            //    mockDbSet.Setup(m => m.Where(predicate)).Returns(data.Where(predicate));

            return mockDbSet;
        }

        private Mock<IUnitOfWork> MockUnitOfWorkSecurable<T>(DbSet<T> dbSet, string keyName) where T : class
        {
            var mockRepo = new Mock<IRepository<T>>();
            mockRepo.Setup(r => r.Query()).Returns(dbSet.AsQueryable);
            mockRepo.Setup(r => r.QueryNoTracking()).Returns(dbSet.AsQueryable);
            mockRepo.Setup(m => m.GetById(It.IsAny<object[]>())).Returns<object[]>(ids => FirstOrDefault(dbSet.AsQueryable(), keyName, ids[0]));
           
           

            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(c => c.Repository<T>()).Returns(mockRepo.Object);
            mockUnitOfWork.Setup(c => c.SaveChanges()).Returns(1);
            return mockUnitOfWork;
        }

        private T FirstOrDefault<T>(IQueryable<T> query, string property, object id)
        {
            ParameterExpression pe = Expression.Parameter(typeof(T), "c");
            Expression columnNameProperty = Expression.Property(pe, property);
            var convertExpression = Expression.Convert(columnNameProperty, typeof(int));

            var idValue = int.Parse(id.ToString());
            var someValueContain = Expression.Constant(idValue, idValue.GetType());

            var equalExpression = Expression.Equal(convertExpression, someValueContain);

            var expression = Expression.Call(
                typeof(Queryable),
                "Where",
                new Type[] { query.ElementType }
                , query.Expression
                , Expression.Lambda<Func<T, bool>>(equalExpression, new ParameterExpression[] { pe }));

            return query.Provider.CreateQuery<T>(expression).FirstOrDefault();
        }
       

        protected Mock<IUnitOfWork> MockUnitOfWorkSecurable<T>(IQueryable<T> data, string keyName) where T : class
        {
            var mockDbSet = MockDbSetSecurable(data);

            return MockUnitOfWorkSecurable(mockDbSet.Object, keyName);
        }
       
    }
}
