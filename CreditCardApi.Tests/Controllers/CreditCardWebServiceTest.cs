﻿using CreditCard.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CreditCard.ServiceImpl.Validators;
using Shouldly;

namespace CreditCardApi.Tests.Controllers
{
    [TestClass]
    public class CreditCardWebServiceTest : BaseTest
    {
        #region Test Methods

        [DataTestMethod]
        [DataRow("4123567890123456", "112016")]
        public void ValidVisa(string cardNumber, string expiredDate)
        {
            //Arrange 
            var testData = new CreditCardModel(cardNumber, expiredDate);

            var service = new CreditCardValidator();
            //Act
            var result = service.Validate(testData);

            //Asset
            result.Errors.Count.ShouldBe(0);
            result.HasError.ShouldBe(false);
        }

        [DataTestMethod]
        [DataRow("5123567890123456", "112003")]
        public void ValidMasterCard(string cardNumber, string expiredDate)
        {
            //Arrange 
            var testData = new CreditCardModel(cardNumber, expiredDate);

            var service = new CreditCardValidator();
            //Act
            var result = service.Validate(testData);

            //Asset
            result.Errors.Count.ShouldBe(0);
            result.HasError.ShouldBe(false);
        }

        [DataTestMethod]
        [DataRow("342356789012345", "112003")]
        [DataRow("372356789012345", "112003")]
        public void ValidAmex(string cardNumber, string expiredDate)
        {
            //Arrange 
            var testData = new CreditCardModel(cardNumber, expiredDate);

            var service = new CreditCardValidator();
            //Act
            var result = service.Validate(testData);

            //Asset
            result.Errors.Count.ShouldBe(0);
            result.HasError.ShouldBe(false);
        }

        [DataTestMethod]
        [DataRow("3528–35890123456", "112003")]
        public void ValidJCB(string cardNumber, string expiredDate)
        {
            //Arrange 
            var testData = new CreditCardModel(cardNumber, expiredDate);

            var service = new CreditCardValidator();
            //Act
            var result = service.Validate(testData);

            //Asset
            result.Errors.Count.ShouldBe(0);
            result.HasError.ShouldBe(false);
        }

        [DataTestMethod]
        [DataRow("4123567890123456", "112015")]
        public void InValidVisa(string cardNumber, string expiredDate)
        {
            //Arrange 
            var testData = new CreditCardModel(cardNumber, expiredDate);

            var service = new CreditCardValidator();
            //Act
            var result = service.Validate(testData);

            //Asset
            result.Errors.Count.ShouldBe(1);
            result.HasError.ShouldBe(true);
            result.Errors.ShouldContain(s => s.ErrorMessageKey == "WrongExpireYear");
            result.Errors.ShouldContain(s => string.Equals(s.ErrorMessageText, "Visa card has expiry year is a leap year", System.StringComparison.OrdinalIgnoreCase));
        }

        [DataTestMethod]
        [DataRow("5123567890123456", "112004")]
        public void InValidMasterCard(string cardNumber, string expiredDate)
        {
            //Arrange 
            var testData = new CreditCardModel(cardNumber, expiredDate);

            var service = new CreditCardValidator();
            //Act
            var result = service.Validate(testData);

            //Asset
            result.Errors.Count.ShouldBe(1);
            result.HasError.ShouldBe(true);
            result.Errors.ShouldContain(s => s.ErrorMessageKey == "WrongExpireYear");
            result.Errors.ShouldContain(s => string.Equals(s.ErrorMessageText, "Master card is the card number where expiry year is a prime number", System.StringComparison.OrdinalIgnoreCase));
        }


        [DataTestMethod]
        [DataRow("3423567890123454", "112003")]
        [DataRow("3723567890123454", "112003")]
        public void InValidAmex(string cardNumber, string expiredDate)
        {
            //Arrange 
            var testData = new CreditCardModel(cardNumber, expiredDate);

            var service = new CreditCardValidator();
            //Act
            var result = service.Validate(testData);

            //Asset
            result.Errors.Count.ShouldBe(1);
            result.HasError.ShouldBe(true);
            result.Errors.ShouldContain(s => s.ErrorMessageKey == "AmexCardWrongFormat");
            result.Errors.ShouldContain(s => string.Equals(s.ErrorMessageText, "Only Amex card number has 15 digits", System.StringComparison.OrdinalIgnoreCase));
        }

        [DataTestMethod]
        [DataRow("3528–3589012345", "112003")]
        public void InValidJCB(string cardNumber, string expiredDate)
        {
            //Arrange 
            var testData = new CreditCardModel(cardNumber, expiredDate);

            var service = new CreditCardValidator();
            //Act
            var result = service.Validate(testData);

            //Asset
            result.Errors.Count.ShouldBe(1);
            result.HasError.ShouldBe(true);
            result.Errors.ShouldContain(s => s.ErrorMessageKey == "CardWrongFormat");
            result.Errors.ShouldContain(s => string.Equals(s.ErrorMessageText, "This card number should be 16 digits", System.StringComparison.OrdinalIgnoreCase));
        }
        #endregion
    }
}
