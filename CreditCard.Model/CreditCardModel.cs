﻿namespace CreditCard.Model
{
    public class CreditCardModel
    {
        public CreditCardModel(string cardNumber, string expireDate)
        {
            CardNumber = cardNumber;
            ExpireDate = expireDate;
        }

        public string CardNumber { get; set; }

        public string ExpireDate { get; set; }
    }
}
