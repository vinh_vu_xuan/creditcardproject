﻿using CreditCard.Core.Data;

namespace CreditCard.Model
{
    public class CreditCardResult
    {
        public CreditCardResult()
        {
            
        }

        public string Result { get; set; }

        public string CardType { get; set; }

        public string ErrorMessage { get; set; }
    }
}
