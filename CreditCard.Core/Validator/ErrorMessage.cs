﻿namespace CreditCard.Core.Validator
{
    public class ErrorMessage
    {
        public ErrorMessage()
        {
        }

        public ErrorMessage(string errorMessageKey)
        {
            ErrorMessageKey = errorMessageKey;
        }

        public ErrorMessage(string errorMessageKey, string errorMessageText)
        {
            ErrorMessageKey = errorMessageKey;
            ErrorMessageText = errorMessageText;
        }

        public ErrorMessage(string errorMessageKey, string errorMessageText, ErrorMessageType errorMessageType)
        {
            ErrorMessageKey = errorMessageKey;
            ErrorMessageText = errorMessageText;
            ErrorMessageType = errorMessageType;
        }

        public ErrorMessageType ErrorMessageType { get; set; } = ErrorMessageType.Error;
        public string ErrorMessageKey { get; set; }
        public string ErrorMessageText { get; set; } = string.Empty;

        public bool HasMessageKey()
        {
            return string.IsNullOrWhiteSpace(ErrorMessageKey);
        }

        public bool HasMessageText()
        {
            return string.IsNullOrWhiteSpace(ErrorMessageText);
        }
    }

    public enum ErrorMessageType
    {
        Error = 1,
        Info = 2,
        Warning = 3
    }
}
