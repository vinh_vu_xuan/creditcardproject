﻿using System.Collections.Generic;
using System.Linq;

namespace CreditCard.Core.Validator
{
    public class ServiceResult
    {
        public ServiceResult()
        {
            Errors = new List<ErrorMessage>();
        }

        public bool HasError => Errors.Any(s => s.ErrorMessageType == ErrorMessageType.Error);
        public bool HasErrorOnly => Errors.Count(s => s.ErrorMessageType == ErrorMessageType.Error) == Errors.Count;
        public bool HasWarning => Errors.Any(s => s.ErrorMessageType == ErrorMessageType.Warning);
        public bool HasWarningOnly => Errors.Count(s => s.ErrorMessageType == ErrorMessageType.Warning) == Errors.Count;

        public List<ErrorMessage> Errors { get; }

        public bool Succeeded { get; private set; }
        public dynamic ReturnedObject { get; private set; }


        public static ServiceResult Success(dynamic returnedObject = null)
        {
            var result = new ServiceResult
            {
                Succeeded = true,
                ReturnedObject = returnedObject
            };
            return result;
        }

        public static ServiceResult Warning(List<ErrorMessage> warnings, dynamic returnedObject = null)
        {
            var result = new ServiceResult { Succeeded = false, ReturnedObject = returnedObject };
            if (warnings != null)
                result.Errors.AddRange(warnings);
            return result;
        }

        public static ServiceResult Failed(ErrorMessage error)
        {
            var result = new ServiceResult { Succeeded = false };
            if (error != null)
                result.Errors.Add(error);
            return result;
        }
        public static ServiceResult Failed(List<ErrorMessage> errors)
        {
            var result = new ServiceResult { Succeeded = false };
            if (errors != null)
                result.Errors.AddRange(errors);
            return result;
        }

        public static ServiceResult Failed()
        {
            var result = new ServiceResult { Succeeded = false };
            return result;
        }

        public object GetReturnedObjectValue(string propertyName)
        {
            if (ReturnedObject == null)
                return null;
            return ReturnedObject.GetType().GetProperty(propertyName).GetValue(ReturnedObject, null);
        }
    }
}
