﻿using System.Threading.Tasks;

namespace CreditCard.Core.Validator
{
    public interface IValidator<in T>
    {
        ServiceResult Validate(T model);
    }
}
