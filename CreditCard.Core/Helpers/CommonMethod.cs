﻿using System;
using CreditCard.Core.Data;

namespace CreditCard.Core.Helpers
{
    public class CommonMethod
    {
        public static bool IsLeapYear(int year)
        {
            return DateTime.IsLeapYear(year);
        }

        public static bool IsPrime(int number)
        {
            if (number <= 1) return false;
            if (number == 2) return true;
            if (number % 2 == 0) return false;

            var boundary = (int)Math.Floor(Math.Sqrt(number));

            for (int i = 3; i <= boundary; i += 2)
                if (number % i == 0)
                    return false;

            return true;
        }

        public static EnumData.CardType GetCardType(string cardType)
        {
            if (cardType.Substring(0, 1) == "4")
                return EnumData.CardType.Visa;
            if (cardType.Substring(0, 1) == "5")
                return EnumData.CardType.Master;
            if (cardType.Substring(0, 2) == "34" || cardType.Substring(0, 2) == "37")
            {
                return EnumData.CardType.Amex;
            }
            if (cardType.Substring(0, 9) == "3528–3589")
                return EnumData.CardType.JCB;
            return EnumData.CardType.Unknown;
        }
    }
}
