﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace CreditCard.Core.Data
{
    public static class EnumData
    {
        public enum Result
        {
            [Display(Name = "Valid")]
            Valid = 0,
            [Display(Name = "Invalid")]
            Invalid = 1,
            [Display(Name = "Does not exist")]
            DoesNotExist = 2
        }

        public enum CardType
        {
           Visa = 1,
           Master = 2,
           Amex = 3,
           JCB = 4,
           Unknown = 5
        }
    }
}
