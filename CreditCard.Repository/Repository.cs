﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CreditCard.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        #region Fields

        private readonly DbSet<T> _dbSet;

        #endregion

        #region Constructors

        public Repository(DbSet<T> dbSet)
        {
            _dbSet = dbSet;
        }

        #endregion

        #region IRepository Implementation

        #region Get & Query

        public IQueryable<T> Query()
        {
            return _dbSet.AsQueryable();
        }

        public IQueryable<T> QueryNoTracking()
        {
            return _dbSet.AsNoTracking();
        }

        public T GetById(params object[] ids)
        {
            return _dbSet.Find(ids);
        }
      

        #endregion

        #endregion
    }
}
