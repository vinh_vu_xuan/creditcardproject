﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;

namespace CreditCard.Repository
{
    public class UnitOfWork : IUnitOfWork
    {

        #region Constructors

        public UnitOfWork(DbContext context)
        {
            _context = context;
            _reps = new Dictionary<string, object>();
        }

        #endregion

        #region Fields

        private readonly DbContext _context;
        private readonly Dictionary<string, object> _reps;
        private DbContextTransaction _dbContextTransaction;

        #endregion

        #region Init Repository

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public IRepository<T> Repository<T>() where T : class
        {
            var name = typeof(IRepository<T>).FullName;
            if (!string.IsNullOrEmpty(name))
            {
                if (!_reps.ContainsKey(name))
                    _reps[name] = Activator.CreateInstance(typeof(Repository<T>), _context.Set<T>());

                return _reps[name] as IRepository<T>;
            }

            return null;
        }

        #endregion

        #region Dispose

        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
                if (disposing)
                    _context.Dispose();

            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        #region Transaction

        public void BeginTransaction()
        {
            _dbContextTransaction = _context.Database.BeginTransaction();
        }

        public void BeginTransaction(IsolationLevel isolationLevel)
        {
            _dbContextTransaction = _context.Database.BeginTransaction(isolationLevel);
        }

        public void Commit()
        {
            try
            {
                // commit transaction if there is one active
                _dbContextTransaction?.Commit();
            }
            catch
            {
                // rollback if there was an exception
                _dbContextTransaction?.Rollback();

                throw;
            }
        }

        public void Rollback()
        {
            _dbContextTransaction?.Rollback();
        }

        #endregion
    }
}