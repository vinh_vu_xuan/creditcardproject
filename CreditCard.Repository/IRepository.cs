﻿using System.Linq;

namespace CreditCard.Repository
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> Query();

        IQueryable<T> QueryNoTracking();

        T GetById(params object[] ids);
    }
}
