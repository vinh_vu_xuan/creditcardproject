﻿using System.Data;

namespace CreditCard.Repository
{
    public interface IUnitOfWork
    {
        void BeginTransaction();

        void BeginTransaction(IsolationLevel isolationLevel);

        void Commit();

        void Rollback();

        int SaveChanges();

        IRepository<T> Repository<T>() where T : class;
    }
}
