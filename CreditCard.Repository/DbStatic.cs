﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Dapper;

namespace CreditCard.Repository
{
    public static class DbStatic
    {
        private static string _applicationName;
        private static string _smartConnectionString;

        internal static string ApplicationName
        {
            get
            {
                if (!string.IsNullOrEmpty(DbStatic._applicationName))
                    return DbStatic._applicationName;
                return DbStatic._applicationName = ConfigurationManager.AppSettings["applicationName"] ?? "NOTDEFINED";
            }
        }

        private static string SmartConnectionString
        {
            get
            {
                if (!string.IsNullOrEmpty(DbStatic._smartConnectionString))
                    return DbStatic._smartConnectionString;
                DbStatic._smartConnectionString = DbStatic.GetSmartConnectionString();
                return DbStatic._smartConnectionString;
            }
        }

        private static string GetSmartConnectionString()
        {
            string str = (string)null;
            Regex regex1 = new Regex("provider\\sconnection\\sstring=\"(?<conn>[^\"]+)");
            Regex regex2 = new Regex("(?<conn>(data|Data)[^\"]+)");
            foreach (object connectionString in (ConfigurationElementCollection)ConfigurationManager.ConnectionStrings)
            {
                if (connectionString.ToString().IndexOf("CreditCard", StringComparison.InvariantCultureIgnoreCase) > 0)
                {
                    str = regex1.Match(connectionString.ToString()).Groups["conn"].Value;
                    if (string.IsNullOrEmpty(str))
                        str = regex2.Match(connectionString.ToString()).Groups["conn"].Value;
                    if (!string.IsNullOrEmpty(str))
                        return str;
                }
            }
            //if (string.IsNullOrEmpty(str))
            //    str = string.Format("Data Source=", (object)DbStatic.ApplicationName);
            return str;
        }

        public static T QuerySingle<T>(string query, object parameters, bool storedProc = true)
        {
            using (SqlConnection cnn = new SqlConnection(DbStatic.SmartConnectionString))
            {
                CommandType? commandType = storedProc ? new CommandType?(CommandType.StoredProcedure) : new CommandType?();
                return cnn.Query<T>(query, parameters, (IDbTransaction)null, true, new int?(), commandType).FirstOrDefault<T>();
            }
        }

        public static IEnumerable<T> Query<T>(string query, object parameters, bool storedProc = true)
        {
            using (SqlConnection cnn = new SqlConnection(DbStatic.SmartConnectionString))
            {
                CommandType? commandType = storedProc ? new CommandType?(CommandType.StoredProcedure) : new CommandType?();
                return cnn.Query<T>(query, parameters, (IDbTransaction)null, true, new int?(), commandType);
            }
        }

        public static void Execute(string query, object parameters)
        {
            using (SqlConnection cnn = new SqlConnection(DbStatic.SmartConnectionString))
                cnn.Execute(query, parameters, (IDbTransaction)null, new int?(), new CommandType?(CommandType.StoredProcedure));
        }
    }
}
