﻿using CreditCard.Core.Validator;
using CreditCard.Model;

namespace CreditCard.ServiceImpl.Validators
{
    public interface ICreditCardValidator : IValidator<CreditCardModel>
    {
    }
}
