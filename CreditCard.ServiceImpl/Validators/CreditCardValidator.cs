﻿using CreditCard.Core.Data;
using CreditCard.Core.Helpers;
using CreditCard.Core.Validator;
using CreditCard.Model;

namespace CreditCard.ServiceImpl.Validators
{
    public class CreditCardValidator : ICreditCardValidator
    {
        public ServiceResult Validate(CreditCardModel model)
        {
            var result = new ServiceResult();

            if (model.CardNumber.Length > 16 || model.CardNumber.Length < 15)
                result.Errors.Add(new ErrorMessage("CardNumberLength", "Card Number Length is invalid"));

            if (model.ExpireDate.Length != 6)
                result.Errors.Add(new ErrorMessage("ExpireDateLength", "Expire Date Length is invalid"));

            var cardType = CommonMethod.GetCardType(model.CardNumber);

            if (model.CardNumber.Length != 15 && cardType == EnumData.CardType.Amex)
                result.Errors.Add(new ErrorMessage("AmexCardWrongFormat", "Only Amex card number has 15 digits"));


            if (model.CardNumber.Length != 16 && cardType != EnumData.CardType.Amex)
                result.Errors.Add(new ErrorMessage("CardWrongFormat", "This card number should be 16 digits"));

            if (model.ExpireDate.Length == 6)
            {
                int.TryParse(model.ExpireDate.Substring(2, 4), out var year);
                if (cardType == EnumData.CardType.Visa && !CommonMethod.IsLeapYear(year))
                {
                    result.Errors.Add(new ErrorMessage("WrongExpireYear", "Visa card has expiry year is a leap year"));
                }

                if (cardType == EnumData.CardType.Master && !CommonMethod.IsPrime(year))
                {

                    result.Errors.Add(new ErrorMessage("WrongExpireYear",
                        "Master card is the card number where expiry year is a prime number"));

                }
            }

            return result;
        }

    }
}
