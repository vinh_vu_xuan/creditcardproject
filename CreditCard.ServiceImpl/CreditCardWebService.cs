﻿using System.Linq;
using CreditCard.Core.Data;
using CreditCard.Core.Extenstions;
using CreditCard.Core.Helpers;
using CreditCard.Core.Validator;
using CreditCard.Model;
using CreditCard.Repository;
using CreditCard.ServiceCtrs;
using CreditCard.ServiceImpl.Validators;

namespace CreditCard.ServiceImpl
{
    public class CreditCardWebService : CreditCardBaseService, ICreditCardService
    {
        private readonly ICreditCardValidator _validator;
        public CreditCardWebService(IUnitOfWork uow, ICreditCardValidator validator) : base(uow)
        {
            _validator = validator;
        }

        #region Get
        public ServiceResult Get(CreditCardModel model)
        {
            var serviceResult = _validator.Validate(model);
            if (serviceResult.HasError)
                return serviceResult;

            var cardType = CommonMethod.GetCardType(model.CardNumber);

            var creditCard = CreditCardRepository.QueryNoTracking().FirstOrDefault(x=> x.ExpiredDate == model.ExpireDate && x.CardNumber == model.CardNumber);
            if (creditCard == null || cardType != EnumData.CardType.JCB)
            {
                return ServiceResult.Success(new CreditCardResult
                {
                    CardType = cardType.ToString(),
                    Result = EnumData.Result.DoesNotExist.GetDisplayName()
                });
            }

            return ServiceResult.Success(new CreditCardResult
            {
                CardType = cardType.ToString(),
                Result = EnumData.Result.Valid.ToString()
            });
        }
        #endregion
    }
}
