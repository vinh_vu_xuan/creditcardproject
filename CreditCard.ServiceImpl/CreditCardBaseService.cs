﻿using CreditCard.Repository;
using CreditCard.DAL;
namespace CreditCard.ServiceImpl
{
    public class CreditCardBaseService : BaseService
    {
        #region Fields

        protected readonly IRepository<DAL.CreditCard> CreditCardRepository;

        #endregion
        public CreditCardBaseService(IUnitOfWork uow) : base(uow)
        {
            CreditCardRepository = Uow.Repository<DAL.CreditCard>();
        }
        

       
    }
}
