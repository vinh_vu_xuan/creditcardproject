﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CreditCard.Repository;

namespace CreditCard.ServiceImpl
{
    public abstract class BaseService
    {
        #region Fields

        protected IUnitOfWork Uow;

        #endregion

        #region Constructors

        protected BaseService(IUnitOfWork uow)
        {
            Uow = uow;
        }

        #endregion
    }

}
